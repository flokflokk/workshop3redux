import React, { Component } from "react";
import HomeRoute from "./view/home/HomeRoute";
import ProductRoute from "./view/products/ProductRoute";
import AppHeader from "./components/Appheader";
import AuthRoute from "./view/auth/AuthRoute";
import { fetchCurrentUser } from './actions/auth'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
// import firebase from "./firebase";

class App extends Component {

  componentDidMount(){
    const { dispatch } = this.props

    dispatch(fetchCurrentUser())
  }

  render() {
    //   const docRef = firebase.db
    //     .collection("products")
    //     .doc("IprpmApLJd6XOScgxgRu");

    //   docRef
    //     .get()
    //     .then(function(doc) {
    //       if (doc.exists) {
    //         console.log("Document data:", doc.data());
    //       } else {
    //         // doc.data() will be undefined in this case
    //         console.log("No such document!");
    //       }
    //     })
    //     .catch(function(error) {
    //       console.log("Error getting document:", error);
    // });
    return (
      <div className="App">
        <AppHeader />
        <AuthRoute  />
        <HomeRoute />
        <ProductRoute />
      </div>
    );
  }
}

export default withRouter(connect()(App))
