import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const config = {
  apiKey: "AIzaSyCr9An5N5HRVy4uXo6bRenYQlwaDyp0ByU",
  authDomain: "workshop3-react.firebaseapp.com",
  databaseURL: "https://workshop3-react.firebaseio.com",
  projectId: "workshop3-react",
  storageBucket: "workshop3-react.appspot.com",
  messagingSenderId: "450167203129"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

db.settings({
  timestampsInSnapshots: true
});

export default {
  db,
  auth,
  storage
};
